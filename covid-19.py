#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# original author: reil_jo

import os
import sys
import time
import datetime
import argparse

import urllib.request

import numpy as np
import matplotlib.pyplot as pyplot
from matplotlib.widgets import Button

URL_BASE = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series"
data_urls = dict(
    confirmed=URL_BASE + "/time_series_covid19_confirmed_global.csv",
    deaths=URL_BASE + "/time_series_covid19_deaths_global.csv",
    recovered=URL_BASE + "/time_series_covid19_recovered_global.csv"
)

class CovidData(object):
    IDX_STATE = 0
    IDX_COUNTRY = 1
    IDX_LAT = 2
    IDX_LONG = 3
    IDX_COUNT_START = 4
    IDX_COUNT_STOP = -1

    def __init__(self, url, auto_update=True):
        self.url = url
        self.name = url.split("/")[-1].split(".")[0]
        self.data_dir = os.path.join(os.getcwd(), "data")
        self.filename = os.path.join(self.data_dir, self.name + ".csv")
        self.auto_update = auto_update
        self._data = None
        
    def download(self):
        print("downloading %s -> %s" % (self.url, self.filename))
        if not os.path.isdir(self.data_dir):
            os.makedirs(self.data_dir)
        urllib.request.urlretrieve(self.url, self.filename)
        self._data = None

    def _ensure_data(self):
        if self._data:
            return
        
        if not os.path.isfile(self.filename):
            self.download()
        
        with open(self.filename, "r") as fp:
            lines = [line.strip().split(",") for line in fp]
        
        self._header = lines[0]
        # sanitize dates
        dates = []
        last_date = None
        for usdat in self._header[CovidData.IDX_COUNT_START:]:
            m, d, y = map(int, usdat.split("/"))
            y += 2000
            #dates.append("%04d-%02d-%02d" % (y, m, d))
            dates.append("%02d-%02d-%02d" % (y-2000, m, d))
            last_date = y, m, d
        self._header[CovidData.IDX_COUNT_START:] = dates
        
        if self.auto_update:
            last_date = datetime.date(*last_date)
            age = datetime.date.today() - last_date
            want_newer = age > datetime.timedelta(days=1)
            age = (time.time() - os.path.getmtime(self.filename)) / 3600.
            if want_newer and age > 0.5:
                print("trying to get newer data...")
                self.download()
                return self._ensure_data()
            
        self._data = []
        self._counts_by_country = dict()
        for line in lines[1:]:
            # sanitize counts
            line[CovidData.IDX_COUNT_START:] = map(int, map(float, line[CovidData.IDX_COUNT_START:]))            
            
            self._data.append(line)
            country = line[CovidData.IDX_COUNTRY].lower()
            this = np.array(line[CovidData.IDX_COUNT_START:])
            other_regions = self._counts_by_country.get(country)
            if other_regions is None:
                self._counts_by_country[country] = this
            else:
                other_regions += this
    
    @property
    def data(self):
        self._ensure_data()
        return self._data
    @property
    def counts_by_country(self):
        self._ensure_data()
        return self._counts_by_country
    @property
    def header(self):
        self._ensure_data()
        return self._header

    def times(self):
        return self.header[CovidData.IDX_COUNT_START:]

citizens = {
    "Germany": 82790000,
    "Italy": 60480000,
    "Spain": 46660000,
    "France": 66990000,
    "United Kingdom": 66440000,
    "US": 327200000,
    "Netherlands": 17180000,
    "China": 1386000000,
    "Uruguay": 3500000
}
citizens.update(dict([(k.lower(), v) for k, v in citizens.items()]))

class covid19_plotter(object):
    def __init__(self):
        self.colors = []
        [[self.colors.append(c + m) for c in "bgrcmyk"] for m in "ov^<>s*D"]

        pyplot.rcParams['font.size'] = 14
        pyplot.rcParams['lines.linewidth'] = 1
        pyplot.rcParams['lines.markeredgewidth'] = 3
        pyplot.rcParams['lines.markersize'] = 3
        pyplot.rcParams['axes.grid'] = 'True'
        pyplot.rcParams['ps.papersize'] = 'a4'
        pyplot.rcParams['savefig.orientation'] = 'landscape'

        self.settings = [
            # name, unit, default, help
            ("forecast", "DAYS", 4),

            ("show_since_day", "DAYS", 50),
            ("not_normed", "", False),
            
            ("lookback_start", "DAYS", 8, "estimate rate over last X days"),
            ("lookback_step", "DAYS", 4),
            ("lookback_n_steps", "N", 1),

            ("fc_retro_forecast", "DAYS", 4, "how many days should old forceasts go into future"),
            ("fc_retro_lookback", "DAYS", "lookback-start"),
            ("fc_retro_n_days", "DAYS", 14, "for how many days in past to plot forecast from that time"),

            ("rate_est_josef", "", False),
            ("fix_axis", "", False, "on False-to-True edge will store and keep current axis limits for upcoming redraws"),
            
        ]
        self.current_setting = 0
        self._texts = {}
        self.timer = None
        self.settings_fn = "settings"
        self.limit = None

    def run(self):
        no_store = "update,josef_jan,no_auto_update".split(",")
        p = argparse.ArgumentParser(description="plot covid-19 counts")
        p.add_argument("--update", action="store_true", help="force update of downloaded data")
        p.add_argument("--countries", help="which countries to show. or 'all'")
        p.add_argument("--josef-jan", action="store_true", help="use settings from josef/jans original script")
        p.add_argument("--no-load", action="store_true", help="do not load from stored settings")
        p.add_argument("--no-auto-update", action="store_true", help="do not automatically decide to download data")

        to_set_from_others = []
        for i, s in enumerate(self.settings):
            name = s[0]
            args = dict(help="")
            if s[1]: args["metavar"] = s[1]
            if s[2] is False:
                args["action"] = "store_true"
            elif isinstance(s[2], str):
                args["default"] = s[2].replace("_", "-")
                to_set_from_others.append((name, s[2]))
            elif s[2]: args["default"] = s[2]
            if len(s) > 3 and s[3]: args["help"] = s[3]
            
            if "default" in args:
                args["help"] = (args["help"] + " (default: %s)" % args["default"]).strip()
            
            p.add_argument("--" + name.replace("_", "-"), **args)

            s = list(s)
            while len(s) < 4:
                s.append(None)
            s[3] = args["help"]
            self.settings[i] = s
        
        args = p.parse_args()

        no_load = args.no_load
        if args.josef_jan:
            no_load = True
            # josef / jan's settings
            args.forecast = 7
            args.show_since_day = 39
            args.not_normed = False
            args.lookback_start = 4 # over last X days -> last week
            args.lookback_step = 4
            args.lookback_n_steps = 5

        for s, s2 in to_set_from_others:
            v = getattr(args, s)
            if not isinstance(v, str):
                continue
            if s2 == "lookback-end":
                v = args.lookback_start + args.lookback_step * args.lookback_n_steps - 1
            else:
                v = getattr(args, s2.replace("-", "_"))
            setattr(args, s, v)

        if not no_load and os.path.isfile(self.settings_fn):
            for k, v in self.load().items():
                if k == "countries" and args.countries:
                    continue # keep user selection
                if k in no_store:
                    continue
                setattr(args, k, v)
        else:
            if not args.countries:
                args.countries = "Germany,US"
        self.args = args
        
        self.all_data = dict()
        for name, url in data_urls.items():
            data = CovidData(url, auto_update=not self.args.no_auto_update)
            if args.update:
                data.download()
            self.all_data[name] = data

        self.dates = self.all_data["confirmed"].times()    
        self.n_days = len(self.dates)
        print("n_days in database: %d" % self.n_days)

        self.fig = pyplot.figure(1, figsize=(18, 8), dpi=100)
        self.replot()
        pyplot.show()
        
    def replot(self):
        globals().update(vars(self.args))
        n_days = self.n_days
        lookback_end = lookback_start + lookback_step * lookback_n_steps

        forecast_labels = ["+%d" % i for i in range(1, forecast + 1)]
        forecast_x = np.arange(n_days - 1, self.n_days + forecast)    

        pyplot.clf()
        pyplot.title("Infektionen - COVID-19")

        if self.args.countries == "all":
            countries = citizens.keys()
        else:
            countries = map(str.strip, self.args.countries.split(","))

        def rate_est_mean_rate(lookback_data):
            return (lookback_data[1:] / lookback_data[:-1]).mean()
        
        def rate_est_e(lookback_data):
            n_days = len(lookback_data)
            start = lookback_data[0]
            end = lookback_data[-1]
            return np.e ** (np.log(end / float(start)) / n_days)
        
        if rate_est_josef:
            selected_rate_est = rate_est_mean_rate
        else:
            selected_rate_est = rate_est_e
            
        for country in countries:
            country = country.lower()
            n_citizens = citizens[country]

            color = self.colors[sum(map(ord, country)) % len(self.colors)] # country uses same color everytime...
            
            data = self.all_data["confirmed"].counts_by_country[country]
            last_day = data[-1]

            def normed(data):
                if self.args.not_normed:
                    return data
                return np.array(data, dtype=float) / n_citizens * 100

            pyplot.plot(normed(data), "-" + color)

            if forecast > 0:
                for lookback in range(lookback_start, lookback_end, lookback_step):
                    lookback_data = data[-lookback:]
                    rate = selected_rate_est(lookback_data)
                    forecast_data = [last_day * rate ** j for j in range(0, forecast + 1)]
                    pyplot.plot(
                        forecast_x, normed(forecast_data),
                        ":" + color,
                        label="%s, rate past %d days: %.3f" % (country, lookback, rate))

            # do forecast retroperspektives
            for n_back in range(fc_retro_n_days):
                n_back += 1
                lookback_data = data[-fc_retro_lookback - n_back:-n_back]
                rate = selected_rate_est(lookback_data)
                last_day = data[-1 - n_back]

                retro_forecast_x = np.arange(n_days - n_back - 1, n_days - n_back + fc_retro_forecast)
                forecast_data = [last_day * rate ** j for j in range(fc_retro_forecast + 1)]

                pyplot.plot(
                    retro_forecast_x, normed(forecast_data),
                    ":" + color[:-1])
        
        xticks = self.dates + forecast_labels
        pyplot.xticks(range(len(xticks)), xticks, rotation=60)
        axend = n_days + forecast

        if self.limit is not None:
            xmin, xmax, ymin, ymax = self.limit
            pyplot.xlim([xmin, xmax])
            pyplot.ylim([ymin, ymax])
        else:
            pyplot.xlim([show_since_day, axend])
        
        pyplot.legend()
        self.last_axis = pyplot.axis()
        #pyplot.tight_layout(1.0)
        self.show_gui()

    def on_prev(self, ev):
        self.current_setting -= 1
        if self.current_setting < 0:
            self.current_setting = len(self.settings) - 1
        self.update_texts()
    def on_next(self, ev):
        self.current_setting += 1
        if self.current_setting == len(self.settings):
            self.current_setting = 0
        self.update_texts()
        
    def on_plus_btn(self, ev):
        self._change_setting(1)
    def on_minus_btn(self, ev):
        self._change_setting(-1)
    def _change_setting(self, dir):
        s = self.settings[self.current_setting][0]
        old_value = getattr(self.args, s)
        if isinstance(old_value, bool):
            new_value = not old_value
        else:
            new_value = old_value + dir
        setattr(self.args, s, new_value)
        if s == "fix_axis":
            if old_value is False and new_value is True:
                self.limit = self.last_axis
            else:
                self.limit = None
        self.update_texts()
        self.start_replot_timer()

    def on_timer(self):
        self.replot()
        self.timer.stop()

    def start_replot_timer(self):
        if self.timer is not None:
            self.timer.stop()
        else:
            self.timer = self.fig.canvas.new_timer(interval=500)
            self.timer.add_callback(self.on_timer)
        self.timer.start()

    def on_save(self, ev):
        print("saving to %s" % self.settings_fn)
        settings = vars(self.args)
        keys = list(settings.keys())
        keys.sort()
        data = "\n".join(["%s: %r" % (k, settings[k]) for k in keys])
        with open(self.settings_fn, "w") as fp:
            fp.write(data + "\n")
    def load(self):
        print("loading from %s" % self.settings_fn)
        with open(self.settings_fn, "r") as fp:
            lines = fp.readlines()
        settings = dict()
        for line in lines:
            if not line.strip():
                continue
            k, v = line.split(": ", 1)
            settings[k] = eval(v)
        return settings
        
    def show_gui(self):
        pad = 0.007
        w = 0.03
        h = 0.05
        
        y = 1 - h - pad
        x = pad
        
        self.prev_btn = Button(pyplot.axes([x, y, w, h]), "<")
        self.prev_btn.on_clicked(self.on_prev)
        x += w + pad
        
        self.next_btn = Button(pyplot.axes([x, y, w, h]), ">")
        self.next_btn.on_clicked(self.on_next)
        x += w + pad

        self._texts["current_setting"] = pyplot.text(x, y, "", transform=self.fig.transFigure)
        
        x = pad
        y -= h + pad
        
        self.mbtn = Button(pyplot.axes([x, y, w, h]), "-")
        self.mbtn.on_clicked(self.on_minus_btn)
        x += w + pad
        
        self.pbtn = Button(pyplot.axes([x, y, w, h]), "+")
        self.pbtn.on_clicked(self.on_plus_btn)
        x += w + pad

        x = pad
        y -= h + pad
        self.save_btn = Button(pyplot.axes([x, y, w * 2 + pad, h]), "save")
        self.save_btn.on_clicked(self.on_save)
        x += w + pad
        
        self.update_texts()
        
    def update_texts(self):
        for what, widget in self._texts.items():
            if what == "current_setting":
                setting = self.settings[self.current_setting]
                v = getattr(self.args, setting[0])
                if setting[3]:
                    s = "%s: %r --> %s" % (setting[0], v, setting[3])
                else:
                    s = "%s: %r" % (setting[0], v)
            else:
                s = "<unknown %s>" % what
            widget.set_text(s)
        pyplot.draw()
    
if __name__ == '__main__':
    plotter = covid19_plotter()
    plotter.run()
